const { path } = require('@vuepress/shared-utils')

module.exports = (options, ctx) => ({
  alias: {
    '@SearchBox':
      path.resolve(__dirname, 'SearchBox.vue')
  },

  define: {
    SEARCH_MAX_SUGGESTIONS: options.searchMaxSuggestions || 5,
    SEARCH_PATHS: options.test || null
  },

  extendPageData ($page) {
    const {
      _strippedContent
    } = $page


    $page.content = _strippedContent;

  }

})
